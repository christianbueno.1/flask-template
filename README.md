# A simple template of flask with factory pattern, Bootstrap

Include the following technologies:
1. Jinja2
2. python3 virtual environment
3. OS: Linux, fedora-34 KDE

### File structure

```bash
(env) [chris@fedora flask-template]$ tree -I 'env|__pycache__'
.
├── app
│   ├── __init__.py
│   ├── static
│   │   ├── images
│   │   │   └── logo.svg
│   │   ├── scripts
│   │   │   └── login.js
│   │   └── styles
│   │       └── default.css
│   └── templates
│       ├── base.html
│       └── index.html
├── instance
│   └── development.yaml
├── README.md
├── requirements.txt
└── tags-search-information.txt

7 directories, 10 files
(env) [chris@fedora flask-template]$ 
```

### Important
> pip install -r requirements.txt

### Resources
* Markdown, web:commonmark [link](https://commonmark.org/help/)

### Flask run
    export FLASK_APP=appname
    export FLASK_ENV=development
    flask run --host=127.0.0.1 --port=5001

### Database, ORM
* connector: mariaDB connector/Python
* database: mariaDB v. 1.0.7
* ORM: FLASK-SQLAlchemy
```
sudo dnf install mariadb-server mariadb-connector-c-devel python3-devel
pip install mariadb
pip install flask-sqlalchemy
```
